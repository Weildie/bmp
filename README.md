# Stack

BmpLib
MathUtils

## Getting started

### step 1
```bash
git clone https://gitlab.com/Weildie/bmp.git
```

### step 2
Build project with CMake (actual verion for build - Visual Studio 19).

### Documentation
```cpp

int main()
{
    std::ifstream in("../in.bmp", std::ios::binary); // input file
    bmp::Image new_img(in); // create obj
	
	new_img.blue();	// filter
    new_img.blur(1); // filter

    new_img.write("../out.bmp"); //write file
    
}

```
