﻿#include <iostream>
#include "inc/bmp/bmp.hpp"
#include <string>

using namespace std;


int main()
{
    std::ifstream in("../in.bmp", std::ios::binary);

    bmp::Image new_img(in);

    // new_img.blur(1); // filter
    // new_img.writeSegment(getSegment(50, 50, 12, new_img));
    // new_img.rotate(acos(-1) / 2);

    std::string text;
    std::cout << "Write down a word you want to encrypt: " << std::endl;
    std::cin >> text;
    new_img.encrypt(text);
    new_img.decrypt(text.size());

    // works stable with another methods

    new_img.write("../out.bmp");
    
}
