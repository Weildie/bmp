#pragma once
#include <vector>
#include <string>
#include <fstream>
#include "../mathutils/matrix.hpp"

namespace bmp
{
    #pragma pack(1)
    struct Pixel
    {
        unsigned char b;
        unsigned char g;
        unsigned char r;
    };
    #pragma pack()

    #pragma pack(1)
    struct BMPHEADER
    {
        unsigned short    Type;
        unsigned int      Size;
        unsigned short    Reserved1;
        unsigned short    Reserved2;
        unsigned int      OffBits;
    };
    #pragma pack()

    #pragma pack(1)
    struct BMPINFO
    {
        unsigned int    Size;
        int             Width;
        int             Height;
        unsigned short  Planes;
        unsigned short  BitCount;
        unsigned int    Compression;
        unsigned int    SizeImage;
        int             XPelsPerMeter;
        int             YPelsPerMeter;
        unsigned int    ClrUsed;
        unsigned int    ClrImportant;
    };
    #pragma pack()

    using BitMap = std::vector<std::vector<Pixel>>;
    using BitVec = std::vector<Pixel>;
    using mt::math::Vec2i;
    using mt::math::Vec2d;
    using mt::math::Mat22d;
    using CordMap = std::vector<std::vector<Vec2d>>;
    using CordRow = std::vector<Vec2d>;

    class Image
    {
    private:
        BMPHEADER  bmpHeader;
        BMPINFO bmpInfo;
        BitMap img_matrix;
        CordMap m_coordinates;
        void writeHead(int height, int width, std::ofstream& out);
    public:
        Image(std::ifstream& in);
        ~Image();
        void blue();
        void blur(int deg);
        void write(std::string way);
        void rotate(double angle);
        void writeSegment(const BitMap& new_image);
        void encrypt(std::string text);
        void decrypt(int deep);
        friend BitMap getSegment(const int x, const int y, const int rad, const Image& img);
    };
};