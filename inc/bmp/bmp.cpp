#include "bmp.hpp"
#include <bitset>

bmp::Image::Image(std::ifstream& in) {
    if (!in.is_open())
        throw std::exception("Image is not open!");

    in.read(reinterpret_cast<char*>(&bmpHeader), sizeof(BMPHEADER));
    in.read(reinterpret_cast<char*>(&bmpInfo), sizeof(BMPINFO));

    for (int i = 0; i < bmpInfo.Height; i++)
    {
        bmp::BitVec row_pixels(bmpInfo.Width);
        img_matrix.push_back(row_pixels);

        bmp::CordRow row_cords;
        for (int j = 0; j < bmpInfo.Width; j++)
        {
            Vec2d tmp({ { 
                { static_cast<double>(j) }, 
                { static_cast<double>(i) }
            } });
            row_cords.push_back(tmp);
        }
        m_coordinates.push_back(row_cords);
    }

    for (int i = 0; i < bmpInfo.Height; i++)
    {
        for (int j = 0; j < bmpInfo.Width; j++)
            in.read(reinterpret_cast<char*>(&img_matrix[i][j]), sizeof(Pixel));
        // TODO refactor it
        if ((3 * bmpInfo.Width) % 4 != 0)
        {
            for (int j = 0; j < 4 - 3 * bmpInfo.Width % 4; j++)
            {
                char c;
                in.read(&c, 1);
            }
        }
    }

}

bmp::Image::~Image() {}

void bmp::Image::blue()
{
    for (int i = 0; i < bmpInfo.Height; i++)
        for (int j = 0; j < bmpInfo.Width; j++)
            img_matrix[i][j].b = 240;
}

void bmp::Image::blur(int deg)
{
    BitMap cpy;
    for (int i = 0; i < bmpInfo.Height; i++)
    {
        bmp::BitVec tmp(bmpInfo.Width);
        cpy.push_back(tmp);
    }

    for (int i = 0; i < bmpInfo.Height; i++)
    {
        for (int j = 0; j < bmpInfo.Width; j++)
        {
            BitMap tmp = getSegment(j, i, deg, *this);
            int b = 0, g = 0, r = 0, c = 1;
            for (int k = 0; k < tmp.size(); k++)
            {
                for (int l = 0; l < tmp[k].size(); l++)
                {
                    b += tmp[k][l].b;
                    g += tmp[k][l].g;
                    r += tmp[k][l].r;
                    c++;
                }
            }

            cpy[i][j].b = b / c;
            cpy[i][j].g = g / c;
            cpy[i][j].r = r / c;
        }
    }

    // copy to Objects BitMap
    for (int i = 0; i < bmpInfo.Height; i++)
        for (int j = 0; j < bmpInfo.Width; j++)
            img_matrix[i][j] = cpy[i][j];
}

void bmp::Image::rotate(double angle)
{
    Vec2d T({ {
        {static_cast<double>(bmpInfo.Width / 2)},
        {static_cast<double>(bmpInfo.Height / 2)}
    } });

    for (int i = 0; i < bmpInfo.Height; i++)
        for (int j = 0; j < bmpInfo.Width; j++)
            m_coordinates[i][j] = m_coordinates[i][j] - T;

    Mat22d R({ {
        {cos(angle), sin(angle)},
        {-sin(angle), cos(angle)}
    } });

    for (int i = 0; i < bmpInfo.Height; i++)
        for (int j = 0; j < bmpInfo.Width; j++)
            m_coordinates[i][j] = R * m_coordinates[i][j];

    int maxX = INT_MIN;
    int minX = INT_MAX;
    int maxY = INT_MIN;
    int minY = INT_MAX;
    for (int i = 0; i < bmpInfo.Height; i++)
        for (int j = 0; j < bmpInfo.Width; j++)
        {
            if (maxX < m_coordinates[i][j].get(0, 0))
                maxX = m_coordinates[i][j].get(0, 0);
            if (minX > m_coordinates[i][j].get(0, 0))
                minX = m_coordinates[i][j].get(0, 0);
            if (maxY < m_coordinates[i][j].get(1, 0))
                maxY = m_coordinates[i][j].get(1, 0);
            if (minY > m_coordinates[i][j].get(1, 0))
                minY = m_coordinates[i][j].get(1, 0);
        }

    maxX++;
    minX--;
    maxY++;
    minY--;
    int width = maxX - minX;
    int height = maxY - minY;

    Vec2d shift({ {
        {static_cast<double>(width / 2)},
        {static_cast<double>(height / 2)}
    } });

    for (int i = 0; i < bmpInfo.Height; i++)
        for (int j = 0; j < bmpInfo.Width; j++)
            m_coordinates[i][j] = m_coordinates[i][j] + shift;

    BitMap new_pixels;
    for (int i = 0; i < height; i++)
    {
        bmp::BitVec row_pixels(width);
        for (int j = 0; j < bmpInfo.Width; j++)
        {
            row_pixels.push_back(Pixel({0, 0, 0}));
        }
        new_pixels.push_back(row_pixels);
    }

    CordMap new_cords;
    for (int i = 0; i < height; i++)
    {
        bmp::CordRow row_cords(width);
        for (int j = 0; j < width; j++)
        {
            row_cords.push_back(Vec2d({ {
                {static_cast<double>(j)}, 
                {static_cast<double>(i)}
            } }));
        }
        new_cords.push_back(row_cords);
    }

    for (int i = 0; i < bmpInfo.Height; i++)
        for (int j = 0; j < bmpInfo.Width; j++)
        {
            int x = (int)(m_coordinates[i][j].get(0, 0));
            int y = (int)(m_coordinates[i][j].get(1, 0));
            new_pixels[y][x] = img_matrix[i][j];
        }

    img_matrix = new_pixels;
    m_coordinates = new_cords;
    bmpInfo.Height = height;
    bmpInfo.Width = width;

    // interpolation
    for (int i = 0; i < bmpInfo.Height; i++)
        for (int j = 0; j < bmpInfo.Width; j++)
            if (img_matrix[i][j].b == 0 && img_matrix[i][j].g == 0 && img_matrix[i][j].r == 0)
            {
                BitMap tmp = getSegment(j, i, 1, *this);
                int b = 255, g = 255, r = 255, c = 1;
                for (int k = 0; k < tmp.size(); k++)
                {
                    for (int l = 0; l < tmp[k].size(); l++)
                    {
                        b += tmp[k][l].b;
                        g += tmp[k][l].g;
                        r += tmp[k][l].r;
                        c++;
                    }
                }

                img_matrix[i][j].b = b / c;
                img_matrix[i][j].g = g / c;
                img_matrix[i][j].r = r / c;
            }
}

void bmp::Image::writeHead(int height, int width, std::ofstream& out)
{
    BMPHEADER bmpHeader_new;
    bmpHeader_new.Type = 0x4D42; // Type BMP (from hex)
    bmpHeader_new.Size = 14 + 40 + (3 * width * height);
    if (width % 4 != 0)
        bmpHeader_new.Size += (4 - 3 * width % 4) * height;
    bmpHeader_new.OffBits = 54;
    bmpHeader_new.Reserved1 = 0;
    bmpHeader_new.Reserved2 = 0;
    out.write(reinterpret_cast<char*>(&bmpHeader_new), sizeof(BMPHEADER));

    BMPINFO bmpInfo_new;
    bmpInfo_new.BitCount = 24;
    bmpInfo_new.ClrImportant = 0;
    bmpInfo_new.ClrUsed = 0;
    bmpInfo_new.Compression = 0;
    bmpInfo_new.Height = height;
    bmpInfo_new.Planes = 1;
    bmpInfo_new.Size = 40;
    bmpInfo_new.SizeImage = bmpHeader_new.Size - 54;
    bmpInfo_new.Width = width;
    bmpInfo_new.XPelsPerMeter = 0;
    bmpInfo_new.YPelsPerMeter = 0;
    out.write(reinterpret_cast<char*>(&bmpInfo_new), sizeof(BMPINFO));
}

void bmp::Image::write(std::string way)
{
    std::ofstream out("../out.bmp", std::ios::binary);
    this->writeHead(bmpInfo.Height, bmpInfo.Width, out);

    for (int i = 0; i < bmpInfo.Height; i++)
    {
        for (int j = 0; j < bmpInfo.Width; j++)
            out.write(reinterpret_cast<char*>(&img_matrix[i][j]), sizeof(Pixel));
        if ((3 * bmpInfo.Width) % 4 != 0)
        {
            for (int j = 0; j < 4 - 3 * bmpInfo.Width % 4; j++)
            {
                char c;
                out.write(&c, 1);
            }
        }
    }
}

bmp::BitMap bmp::getSegment(const int x, const int y, const int rad, const bmp::Image& img)
{
    bmp::BitMap tmp;
    for (int i = 0; i < img.bmpInfo.Height; i++)
    {
        std::vector<bmp::Pixel> row;
        for (int j = 0; j < img.bmpInfo.Width; j++)
            if (j >= x - rad && j <= x + rad && i >= y - rad && i <= y + rad)
                row.push_back(img.img_matrix[i][j]);

        if (row.size() != 0)
            tmp.push_back(row);
    }
    return tmp;
}

void bmp::Image::encrypt(std::string text)
{
    for (std::size_t i = 0; i < text.size(); ++i)
    {
        auto bits = std::bitset<8>(text.c_str()[i]);
        for (int j = 0; j < 8; j++)
            img_matrix[i][j].r = bits[j];
    }
}

void bmp::Image::decrypt(int deep)
{
    for (std::size_t i = 0; i < deep; ++i)
    {
        auto bits = std::bitset<8>();
        for (int j = 0; j < 8; j++)
            bits[j] = img_matrix[i][j].r;

        std::cout << static_cast<char>(bits.to_ulong()) << std::endl;
    }
}

void bmp::Image::writeSegment(const BitMap& new_image) 
{
    int h = new_image.size();
    int w = new_image[0].size();

    std::ofstream out("../out.bmp", std::ios::binary);
    this->writeHead(h, w, out);

    for (int i = 0; i < bmpInfo.Height; i++)
    {
        for (int j = 0; j < bmpInfo.Width; j++)
            out.write(reinterpret_cast<char*>(&img_matrix[i][j]), sizeof(Pixel));
        if ((3 * bmpInfo.Width) % 4 != 0)
        {
            for (int j = 0; j < 4 - 3 * bmpInfo.Width % 4; j++)
            {
                char c;
                out.write(&c, 1);
            }
        }
    }
}